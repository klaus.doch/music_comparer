# Music Comparer
Ein kleines Skript, welches die lokalen Tracks mit denen auf Spotify vergleichen soll. Dabei wird zum Beispiel geprüft, ob alle Lieder eines Albums lokal vorhanden sind oder ob die Dauer mit der auf Spotify angegebenen übereinstimmt. Außerdem wird angezeigt, welche Alben noch nicht runtergeladen wurden.

Er kann mit `python -m App` aus dem `src`-Ordner gestartet werden

## Table of contents
- [Music Comparer](#music-comparer)
  - [Table of contents](#table-of-contents)
  - [Prerequisite](#prerequisite)
    - [VS-Code Extensions:](#vs-code-extensions)
  - [Progess](#progess)
    - [Functions](#functions)
    - [Issues](#issues)
    - [Known Bugs](#known-bugs)
    - [TestCases](#testcases)
  - [UML-Diagramm](#uml-diagramm)
    - [Overview](#overview)
    - [Shared](#shared)
    - [ArtistDetector](#artistdetector)
    - [ArtistComparer](#artistcomparer)
  - [Testing](#testing)
  - [Linting](#linting)
  - [Presentation](#presentation)

## Prerequisite
```console
pip install spotipy
pip install audioread
pip install responses

export SPOTIPY_CLIENT_ID=192ff73c6bd242fbab366eba93a6095b
export SPOTIPY_CLIENT_SECRET=6d1573dae0c9433587b558110ac407f5
```
### VS-Code Extensions:
- Markdown Preview Enhanced 
- Markdown All in One
- PlantUML
- Git History
- Python

## Progess
### Functions
- [x] Es wird geprüft, ob alle runtergeladenen Alben auch alle Lieder beinhalten
- [x] Es wird geprüft, ob die Länge die Lieder auch mit der auf Spotify übereinstimmt
- [ ] Es wird geprüft, ob es Alben von Künstlern gibt, die nicht heruntergeladen wurden
- [x] Das Tool kann erkennen, wenn eine Single zu einem Album gehört
- [x] Konfigdatei nutzen
- [ ] SpotifyDownloader anbinden
- Weitere Ideen sind in meinen Notizen zu finden

### Issues
- [x] SOLID-Refactoring
- [x] UML-Diagramm
- [x] Test fixen
- [x] Längenvergleich
- [x] UnitTests & Integrationstests bauen
  - [x] UnitTests
  - [x] Integrationstest
- [x] 'NotImplemented' ansehen
- [x] Konfigdatei 
  - [x] Resolver bauen
  - [x] Daten aus Datei laden
- [x] Code-Todos abarbeiten 
- [x] Menü implementieren
  - [x] CompositeComponents
  - [x] Menu implementieren
  - [x] Code-Todos
  - [x] IntegrationTests fixen
- [x] Nicht vorhandene Alben anzeigen
- [ ] Präsentation vorbereiten
  - [ ] AppConfig + Music-Folder vorbereiten
  - [ ] Struktur planen
- [ ] Menu als pip?

### Known Bugs
- [ ] Die SpotifyApi liefert das Album "Owdf" von LUVRE47 nicht 

### TestCases
- [ ] Prüfen, ob der BasePfad etwas enthält und existiert

## UML-Diagramm

### Overview
```plantuml
@startuml overview

node "UI" {
  node "pip" {
    [Menu.Contract]
    [Menu]
  }
  [Display]
}
[Display] -right-> [Menu.Contract]
[Menu.Contract] <|.. [Menu]

node "Business" {
  [ArtistComparer.Contract]
  [ArtistDetector.Contract]
  [ArtistComparer]
  [ArtistDetector]
}
[ArtistComparer.Contract] <|.. [ArtistComparer]
[ArtistDetector.Contract] <|.. [ArtistDetector]

node "DataAccess" {
  [FileAccess.Contract]
  [Spotify.Contract]
  [Audio.Contract]
  [FileAccess]
  [Spotify]
  [Audio]
}
[FileAccess.Contract] <|.. [FileAccess]
[Spotify.Contract] <|.. [Spotify]
[Audio.Contract] <|.. [Audio]

node "CrossCutting" {
  [AppData.Contract]
  [AppData]
  [Interaction.Contract]
  [Interaction]
}
[AppData.Contract] <|.. [AppData]
[Interaction.Contract] <|.. [Interaction]

App -down-> [Display]

UI -down-> Business
Business -down-> DataAccess
UI -right-> CrossCutting
Business -right-> CrossCutting
DataAccess -right- CrossCutting



@enduml
```
### Shared
```plantuml
@startuml shared

package Shared <<Frame>> {
  interface IComparer {
    compare()
  }
  class ComparerOutputDecorator implements IComparer {
    IOutput output
    IComparer comparer
    compare()
  }
  interface IFileAccess {
    {abstract} getDirectories()
    {abstract} getFilesByPattern()
    {abstract} joinPaths()
  }
  class FileAcces implements IFileAccess {
    getDirectories()
    getFilesByPattern()
    joinPaths()
  }
  interface IAudio {
    {abstract} getDuration()
  }
  class AudioReadAdapter implements IAudio {
    getDuration()
    IFileAccess fileAccess
  }
  interface IOutput {
    {abstract} send()
    {abstract} sendList()
    {abstract} end()
  }
  interface ISelector {
    {abstract} choose()
  }
  class Terminal implements IOutput, ISelector {
    send()
    sendList()
    end()
    choose()
  }
  interface IApplicationData {
    {abstract} String fileBasePath
    {abstract} Bool logging
    {abstract} String baseType
    {abstract} String compareType
  }
  class ApplicationData implements IApplicationData {
    String fileBasePath
    Bool logging
    String baseType
    String compareType
  }
  interface ISpotify {
    {abstract} searchArtist()
    {abstract} getAlbums()
    {abstract} getTracks()
  }
  class Spotify implements ISpotify {
    searchArtist()
    getAlbums()
    getTracks()
  }

  package SpotifyDataClasses <<Frame>> {
    class SearchResult {
      String id
      String name
    }
    class Album {
      String id
      String name
    }
    class Track {
      String id
      String name
    }
  }
}

ComparerOutputDecorator --> IComparer
ComparerOutputDecorator --> IOutput

AudioReadAdapter --> IFileAccess

ISpotify --> SpotifyDataClasses

@enduml
```
### ArtistDetector
```plantuml
@startuml artistDetector

package ArtistDetector <<Frame>> {
  interface IArtistDetector {
    {abstract} getArtists()
  }
  class FileArtistDetector implements IArtistDetector {
    getArtists()
    IApplicationData appData
    IFileAccess fileAccess
  }
}

package Shared <<Frame>> {
  interface IFileAccess {
    {abstract} getDirectories()
    {abstract} getFilesByPattern()
    {abstract} joinPaths()
  }
  interface IApplicationData {
    {abstract} String fileBasePath
    {abstract} Bool logging
    {abstract} String baseType
    {abstract} String compareType
  }
}
FileArtistDetector --> IFileAccess
FileArtistDetector --> IApplicationData

@enduml
```

### ArtistComparer
```plantuml
@startuml artistComparer

package Shared <<Frame>> {
  interface IComparer
  interface IOutput
  interface ISpotify
  interface IFileAccess
  interface ISelector
  interface IApplicationData
  interface IAudio
}

package ArtistComparer <<Frame>> {
  class ArtistComparer implements IComparer
  class ArtistLoaderFactory implements IArtistLoaderFactory
  class SpotifyArtistLoader implements IArtistLoader
  class FileArtistLoader implements IArtistLoader
  class AlbumComparer implements IAlbumComparer
  class AlbumComparerOutputDecorator implements IAlbumComparer
  package DataClasses <<Frame>> {
    class Artist {
      String name
      List[Album] albums
    }
    class Album {
      String name
      List[Track] tracks
    }
    class Track {
      String name
      Integer duration
    }
  }
}

ArtistComparer --> IArtistLoaderFactory
ArtistComparer --> IAlbumComparer

IArtistLoaderFactory --> IArtistLoader

ArtistLoaderFactory --> IApplicationData

SpotifyArtistLoader --> ISpotify
SpotifyArtistLoader --> ISelector
SpotifyArtistLoader --> IApplicationData

FileArtistLoader --> IFileAccess
FileArtistLoader --> IAudio
FileArtistLoader --> IApplicationData

AlbumComparerOutputDecorator --> IAlbumComparer
AlbumComparerOutputDecorator --> IOutput

Artist *-- Album
Album *-- Track

IArtistLoader : load()

@enduml
```

## Testing
Die Tests habe ich mit Hilfe dieser [Anleitung](https://realpython.com/python-testing/#executing-test-runners) aufgebaut.

Tests können wie folgt ausgeführt werden:
```console
python -m unittest discover -v -s ./test -p "*_Test.py""
```

Hier folgend das Grundgerüst einer Testklasse:
```python
import unittest
from unittest.mock import MagicMock

from 'absolutModulePath' import 'class'


class ClassTest(unittest.TestCase):

    def test_constructor_emptyArguments_assertionError(self):
        with self.assertRaises(AssertionError):
            Class(None, None)

    def test_method_emptyArguments_valueError(self):
        sut = self.createSut()
        with self.assertRaises(ValueError):
            sut.method(None)

    def test_method_validArguments_valueReturned(self):
        sut = self.createSut()

        retVal = sut.method("args")

        self.assertEqual(retVal, 2)

    def createSut(
            self,
            appData=None,
            fileAccess=None) -> Class:

        if appData is None:
            appData = MagicMock(spec=IApplicationData)

        if fileAccess is None:
            fileAccess = self.getDefaultFileAccess()

        return Class(appData, fileAccess)

    def getDefaultFileAccess(self) -> IFileAccess:
        fileAccess = MagicMock(spec=IFileAccess)

        fileAccess.getDirectories.return_value = ["KEIN BOCK", "No face no name"]
        fileAccess.getFilesByPattern.return_value = ["01 KEIN BOCK.m4a", "02 bla.m4a"]

        return fileAccess


if __name__ == '__main__':
    unittest.main()

```

## Linting
Linting kann mit diesem Befehl ausgefühlt werden, sofern dies auch mit `pip` installiert wurde. Allerdings bietet die VS-Code auch Linting an, weshalb ich das darüber mache, da so Einstellungen im Projekt gespeichert werden können.
```console
flake8
```

## Presentation
- Programm zeigen (Mit Aufgabenzettel)
- Erst mal selbst auf Gitlab anschauen lassen & README lesen lassen
- Fragen, was sie überhaupt verstehen oder ob sie Ideen haben
- Entkopplung erklären
- Kurzes Briefing was ist Entwicklung (Prinzipien, Pattern, Modularisierung, TDD)
  - Einführend Wasserfall, Iterativ etc.
- UML-Diagramm
- In Code einsteigen
- Zusammen ein Feature mit TDD umsetzen
  - Vlt der Längenvergleich? Ab `presentation`-Branch starten