from .UI.Display.Display import Display
from .UI.Display.DisplayMenuOutput import DisplayMenuOutput
from .UI.Display.DisplayMenuInput import DisplayMenuInput
from .UI.Menu.MenuManager import MenuManager
from .UI.Menu.MenuBuilder import MenuBuilder

from .Business.ArtistDetector.FileArtistDetector import FileArtistDetector
from .Business.ArtistComparer.ArtistComparer import ArtistComparer
from .Business.ArtistComparer.ComparerOutputDecorator import ComparerOutputDecorator
from .Business.ArtistComparer.ArtistLoader.SpotifyArtistLoader import SpotifyArtistLoader
from .Business.ArtistComparer.ArtistLoader.FileArtistLoader import FileArtistLoader
from .Business.ArtistComparer.ArtistLoader.ArtistLoaderFactory import ArtistLoaderFactory
from .Business.ArtistComparer.AlbumComparer.AlbumComparer import AlbumComparer

from .DataAccess.FileAccess.FileAccess import FileAccess
from .DataAccess.Spotify.Spotify import Spotify
from .DataAccess.Audio.AudioReadAdapter import AudioReadAdapter

from .CrossCutting.Interaction.Terminal import Terminal
from .CrossCutting.AppData.AppDataManager import AppDataManger
from .CrossCutting.Formatter.JsonFormatter import JsonFormatter


class App:

    def __init__(self, appDataPath: str = None) -> None:

        self.buildCompositionRoot(appDataPath)

    # CompositionRoot
    def buildCompositionRoot(self, appDataPath: str) -> None:
        fileAccess = FileAccess()
        formatter = JsonFormatter()
        terminal = Terminal()
        appDataResolver = AppDataManger(fileAccess, formatter, appDataPath)
        spotify = Spotify(appDataResolver)
        audio = AudioReadAdapter(fileAccess)
        spotifyArtistLoader = SpotifyArtistLoader(appDataResolver, spotify, terminal)
        fileArtistLoader = FileArtistLoader(appDataResolver, fileAccess, audio)
        loaderFactory = ArtistLoaderFactory(appDataResolver, [fileArtistLoader, spotifyArtistLoader])
        albumComparer = AlbumComparer()

        artistComparer = ArtistComparer(loaderFactory, albumComparer)
        if appDataResolver.resolve().logging:
            artistComparer = ComparerOutputDecorator(artistComparer, terminal)

        artistDetector = FileArtistDetector(appDataResolver, fileAccess)

        menuManager = MenuManager(DisplayMenuOutput(terminal), DisplayMenuInput(terminal, terminal))

        self.display = Display(menuManager, MenuBuilder(), artistDetector, artistComparer, terminal)

    def run(self):
        self.display.start()
