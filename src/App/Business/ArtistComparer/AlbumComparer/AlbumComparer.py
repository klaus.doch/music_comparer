from .IAlbumComparer import IAlbumComparer
from ...ArtistComparer_Contract.DataClasses.Album import Album
from ...ArtistComparer_Contract.DataClasses.Track import Track
from ...ArtistComparer_Contract.CompareResult import CompareResultItem

from typing import List


class AlbumComparer(IAlbumComparer):

    def isTrackAmountEqual(self, base: Album, compare: Album) -> bool:
        self.validate(base, compare)

        return len(base.tracks) == len(compare.tracks)

    def getDifferentTrackNames(self, base: Album, compare: Album) -> List[CompareResultItem]:
        self.validate(base, compare)

        result = []

        for track in base.tracks:
            try:
                self.getCompareTrack(track, compare)
            except StopIteration:
                result.append(CompareResultItem(f"'{base.name}'", [CompareResultItem(f"Track '{track.name}' not found in album.")]))
                continue

        return result

    def getDifferentTrackLengths(self, base: Album, compare: Album) -> List[CompareResultItem]:
        self.validate(base, compare)

        result = []

        for track in base.tracks:
            try:
                compareTrack = self.getCompareTrack(track, compare)
                durationDiff = compareTrack.duration - track.duration

                if durationDiff > 1000 or durationDiff < -1000:
                    result.append(CompareResultItem(f"'{base.name}'", [CompareResultItem(f"Track '{track.name}' has another length than compare track.")]))

            except StopIteration:
                continue

        return result

    def validate(self, base: Album, compare: Album):
        assert isinstance(base, Album)
        assert isinstance(compare, Album)

        self.checkAlbumName(base, compare)

    def checkAlbumName(self, base: Album, compare: Album):
        if base.name != compare.name:
            raise ValueError()

    # Throw StopIteration if nothing found
    def getCompareTrack(self, track: Track, compare: Album) -> Track:
        return next(ct for ct in compare.tracks if ct.name == track.name)
