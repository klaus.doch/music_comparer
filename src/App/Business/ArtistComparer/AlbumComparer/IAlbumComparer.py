from abc import ABC, abstractmethod
from typing import List

from ...ArtistComparer_Contract.DataClasses.Album import Album
from ...ArtistComparer_Contract.CompareResult import CompareResultItem


class IAlbumComparer(ABC):

    @abstractmethod
    def isTrackAmountEqual(self, base: Album, compare: Album) -> bool:
        pass

    @abstractmethod
    def getDifferentTrackNames(self, base: Album, compare: Album) -> List[CompareResultItem]:
        pass

    @abstractmethod
    def getDifferentTrackLengths(self, base: Album, compare: Album) -> List[CompareResultItem]:
        pass
