from ..ArtistComparer_Contract.IComparer import IComparer
from ..ArtistComparer_Contract.CompareResult import CompareResult, CompareResultItem
from ..ArtistComparer_Contract.DataClasses.Artist import Artist
from ..ArtistComparer_Contract.DataClasses.Album import Album
from ..ArtistComparer_Contract.Exceptions.CompareIdentifierNotFoundError import CompareIdentifierNotFoundError
from ..ArtistComparer_Contract.Exceptions.ComparerAlbumNotFoundError import ComparerAlbumNotFoundError
from ..ArtistComparer_Contract.Exceptions.ArtistNotFoundError import ArtistNotFoundError
from .ArtistLoader.IArtistLoaderFactory import IArtistLoaderFactory
from .AlbumComparer.IAlbumComparer import IAlbumComparer


class ArtistComparer(IComparer):

    def __init__(
            self,
            loaderFactory: IArtistLoaderFactory,
            albumComparer: IAlbumComparer) -> None:
        assert isinstance(loaderFactory, IArtistLoaderFactory)
        assert isinstance(albumComparer, IAlbumComparer)

        self.loaderFactory = loaderFactory
        self.albumComparer = albumComparer

    def compareAll(self, artistName: str) -> CompareResult:
        if not artistName:
            raise ValueError('empty artist')

        result = CompareResult()

        try:
            baseArtist = self.loaderFactory.getBaseLoader().load(artistName)
            compareArtist = self.loaderFactory.getCompareLoader().load(artistName)
        except ArtistNotFoundError as error:
            raise CompareIdentifierNotFoundError(error.args[0])

        for baseAlbum in baseArtist.albums:

            try:
                compareAlbum = self.getCompareAlbum(baseAlbum, compareArtist)
            except ComparerAlbumNotFoundError as error:
                result.items.append(CompareResultItem(error.args[0]))
                continue

            if not self.albumComparer.isTrackAmountEqual(baseAlbum, compareAlbum):
                result.items.append(CompareResultItem(f"'{baseAlbum.name}'", [CompareResultItem("Album has different track amount.")]))
                continue

            result.items.extend(self.albumComparer.getDifferentTrackNames(baseAlbum, compareAlbum))
            result.items.extend(self.albumComparer.getDifferentTrackLengths(baseAlbum, compareAlbum))

        return result

    def getNewAlbumsFromCompare(self, artistName):
        if not artistName:
            raise ValueError('empty artist')

        result = CompareResult()

        try:
            baseArtist = self.loaderFactory.getBaseLoader().load(artistName)
            compareArtist = self.loaderFactory.getCompareLoader().load(artistName)
        except ArtistNotFoundError as error:
            raise CompareIdentifierNotFoundError(error.args[0])

        for compareAlbum in compareArtist.albums:
            try:
                self.getCompareAlbum(compareAlbum, baseArtist)
            except ComparerAlbumNotFoundError:
                result.items.append(CompareResultItem(f"Album '{compareAlbum.name}' not found"))

        return result

    def getCompareAlbum(self, baseAlbum: Album, compareArtist: Artist) -> Album:
        matches = list((ca for ca in compareArtist.albums if ca.name == baseAlbum.name))

        if len(matches) == 0:
            raise ComparerAlbumNotFoundError(f"No compare album for album '{baseAlbum.name}' found!")

        if len(matches) > 1:
            raise ValueError()

        return matches[0]
