from ....CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from ...ArtistComparer_Contract.Exceptions.ArtistLoaderNotFoundError import ArtistLoaderNotFoundError
from .IArtistLoader import IArtistLoader
from .IArtistLoaderFactory import IArtistLoaderFactory

from typing import List


class ArtistLoaderFactory(IArtistLoaderFactory):

    def __init__(
            self,
            appDataResolver: IAppDataResolver,
            loaders: List[IArtistLoader]) -> None:
        self.appDataResolver = appDataResolver
        self.loaders = loaders

    def getBaseLoader(self) -> IArtistLoader:
        return self.getLoaderByIdentifier(self.appDataResolver.resolve().comparerBaseType)

    def getCompareLoader(self) -> IArtistLoader:
        return self.getLoaderByIdentifier(self.appDataResolver.resolve().comparerCompareType)

    def getLoaderByIdentifier(self, identifier: str) -> IArtistLoader:
        try:
            return next(loader for loader in self.loaders if loader.getIdentifier() == identifier)
        except StopIteration:
            raise ArtistLoaderNotFoundError()
