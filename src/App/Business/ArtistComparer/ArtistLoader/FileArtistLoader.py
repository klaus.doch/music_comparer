from ....CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from ....DataAccess.FileAccess_Contracts.IFileAccess import IFileAccess
from ....DataAccess.Audio_Contracts.IAudio import IAudio
from ...ArtistComparer_Contract.DataClasses.Artist import Artist
from ...ArtistComparer_Contract.DataClasses.Album import Album
from ...ArtistComparer_Contract.DataClasses.Track import Track
from ...ArtistComparer_Contract.Exceptions.ArtistNotFoundError import ArtistNotFoundError
from .IArtistLoader import IArtistLoader


class FileArtistLoader(IArtistLoader):

    def __init__(
            self,
            appDataResolver: IAppDataResolver,
            fileAccess: IFileAccess,
            audio: IAudio) -> None:

        assert isinstance(appDataResolver, IAppDataResolver)
        assert isinstance(fileAccess, IFileAccess)
        assert isinstance(audio, IAudio)

        self.appDataResolver = appDataResolver
        self.fileAccess = fileAccess
        self.audio = audio

    def load(self, artistName: str) -> Artist:

        fileBasePath = self.appDataResolver.resolve().fileBasePath

        if not artistName:
            raise ValueError('empty artistName')

        artist = Artist(artistName)

        try:
            albumNames = self.fileAccess.getDirectories(fileBasePath, artistName)
        except FileNotFoundError:
            raise ArtistNotFoundError(f"No artist with name '{artistName}' found.")

        for albumName in albumNames:
            album = Album(albumName)

            trackFiles = self.fileAccess.getFilesByPattern(
                fileBasePath,
                artistName,
                albumName,
                pattern="m4a")

            tracks = []
            for track in trackFiles:

                duration = self.audio.getDuration(
                    fileBasePath,
                    artistName,
                    albumName,
                    track)
                tracks.append(Track(track[3:-4], duration))

            album.tracks = tracks
            artist.albums.append(album)

        return artist

    def getIdentifier(self) -> str:
        return "file"
