from abc import ABC, abstractmethod

from ...ArtistComparer_Contract.DataClasses import Artist


class IArtistLoader(ABC):

    @abstractmethod
    def load(self, artistName: str) -> Artist:
        """Can raise 'ArtistLoaderNotFoundError'"""
        pass

    @abstractmethod
    def getIdentifier(self) -> str:
        pass
