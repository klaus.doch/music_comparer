from abc import ABC, abstractmethod

from ..ArtistLoader.IArtistLoader import IArtistLoader


class IArtistLoaderFactory(ABC):

    @abstractmethod
    def getBaseLoader(self) -> IArtistLoader:
        pass

    @abstractmethod
    def getCompareLoader(self) -> IArtistLoader:
        pass
