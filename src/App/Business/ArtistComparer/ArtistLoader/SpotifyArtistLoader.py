from ....CrossCutting.Interaction_Contract.ISelector import ISelector
from ....CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from ....DataAccess.Spotify_Contracts.ISpotify import ISpotify
from ...ArtistComparer_Contract.DataClasses.Artist import Artist
from ...ArtistComparer_Contract.DataClasses.Album import Album
from ...ArtistComparer_Contract.DataClasses.Track import Track
from ...ArtistComparer_Contract.Exceptions.ArtistNotFoundError import ArtistNotFoundError
from .IArtistLoader import IArtistLoader


# Adapter-Pattern, dass so wirklich nur das Interface implementiert wird und die eigentliche Logik in einer eigenen
# Klasse liegt
class SpotifyArtistLoader(IArtistLoader):

    def __init__(
            self,
            appDataResolver: IAppDataResolver,
            spotify: ISpotify,
            selector: ISelector) -> None:

        assert isinstance(appDataResolver, IAppDataResolver)
        assert isinstance(spotify, ISpotify)
        assert isinstance(selector, ISelector)

        self.appData = appDataResolver.resolve()
        self.spotify = spotify
        self.selector = selector

    def load(self, artistName: str) -> Artist:

        if not artistName:
            raise ValueError('empty artistName')

        artistsResult = self.spotify.searchArtist(artistName)

        artistIndex = 0

        if len(artistsResult) == 0:
            raise ArtistNotFoundError(f"No artist with name '{artistName}' found.")
        elif len(artistsResult) > 1:
            artistIndex = self.selector.choose(map(lambda x: x.name, artistsResult), 2)

        artist = Artist(artistsResult[artistIndex].name)

        spotifyAlbums = self.spotify.getAlbums(artistsResult[artistIndex].id)

        for spotifyAlbum in spotifyAlbums:
            album = Album(spotifyAlbum.name)

            spotifyTracks = self.spotify.getTracks(spotifyAlbum.id)

            for spotifyTrack in spotifyTracks:
                album.tracks.append(Track(spotifyTrack.name, spotifyTrack.duration))

            artist.albums.append(album)

        cleanedArtist = self.removeDuplicated(artist)

        return cleanedArtist

    def removeDuplicated(self, artist: Artist) -> Artist:
        duplicatedAlbums = []

        for album in artist.albums:
            for compareAlbum in artist.albums:
                if self.IsAlbumPartOfTheOther(album, compareAlbum):
                    duplicatedAlbums.append(album)
                    break

        for delAlbum in duplicatedAlbums:
            artist.albums.remove(delAlbum)

        return artist

    def IsAlbumPartOfTheOther(self, checkAlbum: Album, containingAlbum: Album) -> bool:
        if checkAlbum == containingAlbum:
            return False

        checkAlbumTrackNames = map(lambda x: x.name, checkAlbum.tracks)
        containingAlbumTrackNames = map(lambda x: x.name, containingAlbum.tracks)

        # Are track names of one album in the other
        if not set(checkAlbumTrackNames) <= set(containingAlbumTrackNames):
            return False

        # Are the lengths of tracks similar
        for checkTrack in checkAlbum.tracks:
            containingTrack = next(track for track in containingAlbum.tracks if track.name == checkTrack.name)
            durationDif = checkTrack.duration - containingTrack.duration
            if durationDif > 5000 or durationDif < -5000:
                return False

        return True

    def getIdentifier(self) -> str:
        return "spotify"
