from typing import List

from ...CrossCutting.Interaction_Contract.IOutput import IOutput
from ..ArtistComparer_Contract.Exceptions.CompareIdentifierNotFoundError import CompareIdentifierNotFoundError
from ..ArtistComparer_Contract.IComparer import IComparer
from ..ArtistComparer_Contract.CompareResult import CompareResult, CompareResultItem


class ComparerOutputDecorator(IComparer):

    def __init__(self, comparer: IComparer, output: IOutput) -> None:
        assert isinstance(comparer, IComparer)
        assert isinstance(output, IOutput)

        self.c = comparer
        self.output = output

    def compareAll(self, artistName: str) -> CompareResult:
        self.output.send("Comparing " + artistName + "...", 0)

        try:
            result = self.c.compareAll(artistName)
        except CompareIdentifierNotFoundError as error:
            result = CompareResult([CompareResultItem(error.args[0])])
            self.output.send(error.args[0], 1)
        else:
            self.printResult(result.items, 1)

        self.output.end()

        return result

    def getNewAlbumsFromCompare(self, artistName: str) -> CompareResult:
        self.output.send("Comparing " + artistName + "...", 0)

        try:
            result = self.c.getNewAlbumsFromCompare(artistName)
        except CompareIdentifierNotFoundError as error:
            result = CompareResult([CompareResultItem(error.args[0])])
            self.output.send(error.args[0], 1)
        else:
            if len(result.items) > 0:
                self.printResult(result.items, 1)
            else:
                self.output.send("No new albums detected.")

        self.output.end()

        return result

    def printResult(self, resultItems: List[CompareResultItem], level: int):

        actualMessage = None

        for item in resultItems:
            if actualMessage != item.message:
                actualMessage = item.message
                self.output.send(actualMessage, level)
            self.printResult(item.items, level + 1)
