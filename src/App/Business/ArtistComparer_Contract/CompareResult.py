from typing import List
from dataclasses import dataclass, field


@dataclass
class CompareResultItem:
    message: str
    items: List['CompareResultItem'] = field(default_factory=list)


@dataclass
class CompareResult:
    items: List[CompareResultItem] = field(default_factory=list)
