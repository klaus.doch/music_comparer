from .Track import Track

from typing import List
from dataclasses import dataclass, field


@dataclass
class Album:

    name: str
    tracks: List[Track] = field(default_factory=list)
