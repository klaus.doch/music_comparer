from .Album import Album

from typing import List
from dataclasses import dataclass, field


@dataclass
class Artist:

    name: str
    albums: List[Album] = field(default_factory=list)
