from ....CrossCutting.Error import Error


class ArtistLoaderNotFoundError(Error):
    """Raised when no an specific artist loader not found"""
    pass
