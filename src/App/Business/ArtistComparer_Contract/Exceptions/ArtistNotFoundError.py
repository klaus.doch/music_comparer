from ....CrossCutting.Error import Error


class ArtistNotFoundError(Error):
    """Raised when the required artist not found"""
    pass
