from ....CrossCutting.Error import Error


class CompareIdentifierNotFoundError(Error):
    """Raised when the given identifier not found"""
    pass
