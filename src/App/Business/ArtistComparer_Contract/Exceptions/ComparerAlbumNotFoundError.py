from ....CrossCutting.Error import Error


class ComparerAlbumNotFoundError(Error):
    """Raised when no compare album found"""
    pass
