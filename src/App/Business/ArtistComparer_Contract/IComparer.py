from abc import ABC, abstractmethod

from .CompareResult import CompareResult


class IComparer(ABC):

    @abstractmethod
    def compareAll(self, artistName: str) -> CompareResult:
        """Can raise 'CompareIdentifierNotFoundError'"""
        pass

    @abstractmethod
    def getNewAlbumsFromCompare(self, artistName: str) -> CompareResult:
        """Can raise 'CompareIdentifierNotFoundError'"""
        pass
