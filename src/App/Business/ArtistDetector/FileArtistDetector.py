from typing import List

from ...CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from ...DataAccess.FileAccess_Contracts.IFileAccess import IFileAccess
from ..ArtistDetector_Contract.IArtistDetector import IArtistDetector
from ..ArtistDetector_Contract.NoArtistFoundError import NoArtistFoundError


class FileArtistDetector(IArtistDetector):

    def __init__(self, appDataResolver: IAppDataResolver, fileAccess: IFileAccess) -> None:
        assert isinstance(appDataResolver, IAppDataResolver)
        assert isinstance(fileAccess, IFileAccess)

        self.appDataResolver = appDataResolver
        self.fileAccess = fileAccess

    def getArtists(self) -> List[str]:
        try:
            return self.fileAccess.getDirectories(self.appDataResolver.resolve().fileBasePath)
        except FileNotFoundError:
            raise NoArtistFoundError(f"No artist under directory '{self.appDataResolver.resolve().fileBasePath}' found.")
