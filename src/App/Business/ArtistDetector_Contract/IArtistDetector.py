from abc import ABC, abstractmethod
from typing import List


class IArtistDetector(ABC):

    @abstractmethod
    def getArtists(self) -> List[str]:
        """Can raise NoArtistFoundError"""
        pass
