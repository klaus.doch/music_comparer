from ...CrossCutting.Error import Error


class NoArtistFoundError(Error):
    """Raised when no artist from detection was found"""
    pass
