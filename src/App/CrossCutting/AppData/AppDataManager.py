from ..AppData_Contracts.IAppDataManager import IAppDataManager
from ..AppData_Contracts.IAppDataResolver import IAppDataResolver
from ..AppData_Contracts.AppData import AppData
from ..Formatter_Contracts.IFormatter import IFormatter
from ..Error import ApplicationError
from ...DataAccess.FileAccess_Contracts.IFileAccess import IFileAccess


class AppDataManger(IAppDataManager, IAppDataResolver):

    def __init__(self, fileAccess: IFileAccess, formatter: IFormatter, appDataPath: str = None):
        isinstance(fileAccess, IFileAccess)
        isinstance(formatter, IFormatter)

        self.fileAccess = fileAccess
        self.formatter = formatter
        self.appData = None

        if appDataPath is None:
            self.path = f"App/appData.{self.formatter.defaultFileEnding}"
        else:
            self.path = appDataPath

    def resolve(self) -> AppData:
        if self.appData is None:
            self.appData = self.load()
        return self.appData

    def setProperty(self, name: str, value):
        # - Neues Property in Config speichern
        # - Config neu in self.appData laden
        # - 'AppDataPropertyNotFoundError' werfen wenn nicht vorhandenes Property geschrieben wird
        # - 'AppDataPropertyTypeNotSupportedError' werfen, wenn value einen Typ hat, der nicht implementiert ist (Also nicht bool oder string ist)
        raise NotImplementedError()

    def load(self) -> AppData:
        try:
            strAppData = self.fileAccess.readFile(self.path)
        except FileNotFoundError:
            raise ApplicationError(f"No AppData found with path '{self.path}'")
        formattedAppData = self.formatter.format(strAppData)
        return AppData(**formattedAppData)
