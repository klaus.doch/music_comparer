from dataclasses import dataclass


@dataclass(frozen=True)
class AppData:
    logging: bool
    fileBasePath: str
    comparerBaseType: str
    comparerCompareType: str
    spotifyClientId: str
    spotifyClientSecret: str
