from abc import ABC, abstractmethod


class IAppDataManager(ABC):

    @abstractmethod
    def setProperty(self, name: str, value):
        """
        Can raise 'AppDataPropertyNotFoundError'
        Can raise 'AppDataPropertyTypeNotSupportedError'
        """
        pass
