from abc import ABC, abstractmethod

from .AppData import AppData


class IAppDataResolver(ABC):

    @abstractmethod
    def resolve(self) -> AppData:
        pass
