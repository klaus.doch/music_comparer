class Error(Exception):
    """Base class for other exceptions"""
    pass


class ApplicationError(Error):
    """Exceptions that crashes the whole application"""
    pass
