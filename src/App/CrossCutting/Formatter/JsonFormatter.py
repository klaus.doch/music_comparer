import json
from typing import Any

from ..Formatter_Contracts.IFormatter import IFormatter


class JsonFormatter(IFormatter):

    def format(self, content: str) -> Any:
        return json.loads(content)

    @property
    def defaultFileEnding(self) -> str:
        return "json"
