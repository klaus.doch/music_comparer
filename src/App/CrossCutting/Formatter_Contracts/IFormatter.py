from abc import ABC, abstractmethod
from typing import TypeVar


T = TypeVar('T')


class IFormatter(ABC):

    @abstractmethod
    def format(self, content: str, type: T) -> T:
        pass

    @property
    @abstractmethod
    def defaultFileEnding(self) -> str:
        pass
