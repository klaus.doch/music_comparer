from typing import List
from os import system, name

from ..Interaction_Contract.ISelector import ISelector
from ..Interaction_Contract.IOutput import IOutput
from ..Interaction_Contract.IInput import IInput
from ..Interaction_Contract.InvalidSelectionError import InvalidSelectionError


class Terminal(IOutput, ISelector, IInput):

    def send(self, message: str, level: int) -> None:
        if level == 2:
            message = "- " + message

        print(message.rjust(len(message) + level*2))

    def sendList(self, messages: List[str], level: int) -> None:
        for message in messages:
            self.send(message, level)

    def end(self) -> None:
        print()

    def choose(self, options: List[str], level: int) -> int:

        self.send("Select between the following options:", level)

        index = 0
        for option in options:
            index += 1
            self.send(str(index) + ". " + option, level + 1)

        try:
            return int(input()) - 1
        except ValueError:
            raise InvalidSelectionError()

    def silentChoose(self):
        try:
            return int(input()) - 1
        except ValueError:
            raise InvalidSelectionError()

    def waitForInput(self) -> None:
        input()

    # Früher oder später betriebssystemtypische Implementierung mit Factory rausziehen
    def clear(self) -> None:
        if name == 'nt':
            system('cls')
        else:
            system('clear')
