from abc import ABC, abstractmethod


class IInput(ABC):

    @abstractmethod
    def waitForInput(self) -> int:
        pass
