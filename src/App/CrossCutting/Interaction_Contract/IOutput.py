from abc import ABC, abstractmethod
from typing import List


class IOutput(ABC):

    @abstractmethod
    def send(self, message: str, level: int) -> None:
        pass

    @abstractmethod
    def sendList(self, messages: List[str], level: int) -> None:
        pass

    @abstractmethod
    def end(self) -> None:
        pass

    @abstractmethod
    def clear(self) -> None:
        pass
