from abc import ABC, abstractmethod
from typing import List


class ISelector(ABC):

    @abstractmethod
    def choose(self, options: List[str], level: int) -> int:
        """Can raise InvalidSelectionError"""
        pass

    def silentChoose(self) -> int:
        """Can raise InvalidSelectionError"""
        pass
