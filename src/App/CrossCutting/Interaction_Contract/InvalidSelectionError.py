from ..Error import Error


class InvalidSelectionError(Error):
    """Raise when invalid value selected"""
    pass
