from ..Audio_Contracts.IAudio import IAudio
from ..FileAccess_Contracts.IFileAccess import IFileAccess

import audioread


class AudioReadAdapter(IAudio):

    def __init__(self, fileAccess: IFileAccess) -> None:

        assert isinstance(fileAccess, IFileAccess)

        self.fileAccess = fileAccess

    def getDuration(self, *paths: str) -> int:
        path = self.fileAccess.joinPaths(*paths)

        with audioread.audio_open(path) as f:
            return f.duration * 1000
