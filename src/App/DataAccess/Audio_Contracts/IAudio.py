from abc import ABC, abstractmethod


class IAudio(ABC):

    @abstractmethod
    def getDuration(self, *paths: str) -> int:
        pass
