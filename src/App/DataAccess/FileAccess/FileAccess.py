from os import listdir
from os.path import isdir, isfile, join
from typing import List, Callable

from ..FileAccess_Contracts.IFileAccess import IFileAccess


class FileAccess(IFileAccess):

    # public
    def getDirectories(self, *paths: str) -> List[str]:

        path = self.joinPaths(*paths)

        return self.getItemsByFilter(path, lambda name, path: isdir(path) and not name.startswith('$'))

    def getFilesByPattern(self, *paths: str, pattern: str) -> List[str]:

        path = self.joinPaths(*paths)

        return self.getItemsByFilter(path, lambda name, path: isfile(path) and name.endswith(pattern))

    def joinPaths(self, *paths: str) -> str:

        fullPath = ""

        for path in paths:
            fullPath = join(fullPath, path)

        return fullPath

    def readFile(self, *paths: str) -> str:

        path = self.joinPaths(*paths)

        with open(path, "r") as file:
            return file.read()

    # private
    def getItemsByFilter(self, path: str, filter: Callable[[str, str], bool]) -> List[str]:
        returnItems = []

        for name in listdir(path):
            combinatedPath = self.joinPaths(path, name)

            if filter(name, combinatedPath):
                returnItems.append(name)

        return returnItems
