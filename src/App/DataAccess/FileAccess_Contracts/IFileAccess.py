from abc import ABC, abstractmethod
from typing import List


class IFileAccess(ABC):

    @abstractmethod
    def getDirectories(self, *paths: str) -> List[str]:
        """Can raise 'FileNotFoundError'"""
        pass

    @abstractmethod
    def getFilesByPattern(self, *paths: str, pattern: str) -> List[str]:
        """Can raise 'FileNotFoundError'"""
        pass

    @abstractmethod
    def joinPaths(self, *paths: str) -> str:
        pass

    @abstractmethod
    def readFile(self, *paths: str) -> str:
        """Can raise 'FileNotFoundError'"""
        pass
