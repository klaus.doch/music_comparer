from typing import List

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

from ..Spotify_Contracts.ISpotify import ISpotify
from ..Spotify_Contracts.DataClasses import SearchResult, Album, Track
from ...CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver


class Spotify(ISpotify):

    def __init__(self, appDataResolver: IAppDataResolver) -> None:
        isinstance(appDataResolver, IAppDataResolver)

        self.appData = appDataResolver.resolve()

        auth_manager = SpotifyClientCredentials(
            client_id=self.appData.spotifyClientId,
            client_secret=self.appData.spotifyClientSecret)
        self.sp = spotipy.Spotify(auth_manager=auth_manager)

    def searchArtist(self, artistName: str) -> List[SearchResult]:
        artists = self.sp.search(artistName, type="artist")

        result = []
        for artist in artists["artists"]["items"]:
            result.append(SearchResult(artist["id"], artist["name"]))

        return result

    def getAlbums(self, artistId: str) -> List[Album]:
        albums = self.sp.artist_albums(artistId)

        result = []
        for album in albums["items"]:
            result.append(Album(album["id"], album["name"]))

        return result

    def getTracks(self, albumId: str) -> List[Track]:
        tracks = self.sp.album_tracks(albumId)

        result = []
        for track in tracks["items"]:
            result.append(Track(track["id"], track["name"], track["duration_ms"]))

        return result
