from dataclasses import dataclass


@dataclass
class SearchResult:
    id: str
    name: str


@dataclass
class Album:
    id: str
    name: str


@dataclass
class Track:
    id: str
    name: str
    duration: int
