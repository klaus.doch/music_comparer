from .DataClasses import SearchResult, Album, Track

from abc import ABC, abstractmethod
from typing import List


class ISpotify(ABC):

    @abstractmethod
    def searchArtist(self, artistName: str) -> List[SearchResult]:
        pass

    @abstractmethod
    def getAlbums(self, artistId: str) -> List[Album]:
        pass

    @abstractmethod
    def getTracks(self, albumId: str) -> List[Track]:
        pass
