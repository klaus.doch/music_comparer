from ...Menu_Contract.ICommand import ICommand
from ....Business.ArtistDetector_Contract.IArtistDetector import IArtistDetector
from ....Business.ArtistComparer_Contract.IComparer import IComparer


class ArtistCompareCommand(ICommand):

    def __init__(
            self,
            artistDetector: IArtistDetector,
            artistComparer: IComparer):
        isinstance(artistDetector, IArtistDetector)
        isinstance(artistComparer, IComparer)

        self.artistDetector = artistDetector
        self.comparer = artistComparer
        self.lastResults = []

    def execute(self):
        artists = self.artistDetector.getArtists()

        for artist in artists:
            result = self.comparer.compareAll(artist)
            self.lastResults.append(result)

    def waitAfterSucceeded(self) -> bool:
        return True
