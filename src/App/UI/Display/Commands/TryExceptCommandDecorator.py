from ...Menu_Contract.ICommand import ICommand
from ....CrossCutting.Error import Error, ApplicationError
from ....CrossCutting.Interaction_Contract.IOutput import IOutput


class TryExceptCommandDecorator(ICommand):

    def __init__(
            self,
            command: ICommand,
            output: IOutput):
        isinstance(command, ICommand)
        isinstance(output, IOutput)

        self.command = command
        self.output = output

    # - ApplicationError führt zum Beenden der Anwendung
    # - Alle anderen User-Definded Exceptions werden ausgegeben und das Menü wird angezeigt
    # - Die restlichen Exceptions werden nicht abgefangen und führen so zum Beenden der Anwendung
    def execute(self):
        try:
            self.command.execute()

        except ApplicationError:
            raise

        except Error as error:
            # Z.B. ArtistNotFoundError
            self.output.send(f"Exception of type '{str(type(error))}' thrown!", 0)
            self.output.sendList(error.args, 2)

    def waitAfterSucceeded(self) -> bool:
        return self.command.waitAfterSucceeded()
