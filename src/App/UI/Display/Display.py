from ...CrossCutting.Interaction_Contract.IOutput import IOutput
from ...Business.ArtistDetector_Contract.IArtistDetector import IArtistDetector
from ...Business.ArtistComparer_Contract.IComparer import IComparer
from ..Menu_Contract.IMenuManager import IMenuManager
from ..Menu_Contract.IMenuBuilder import IMenuBuilder
from ..Menu_Contract.ICommand import ICommand
from .Commands.ArtistCompareCommand import ArtistCompareCommand
from .Commands.DetectNewAlbumsCommand import DetectNewAlbumsCommand
from .Commands.TryExceptCommandDecorator import TryExceptCommandDecorator


class Display:

    def __init__(
            self,
            menuManager: IMenuManager,
            menuBuilder: IMenuBuilder,
            artistDetector: IArtistDetector,
            artistComparer: IComparer,
            output: IOutput):
        isinstance(menuManager, IMenuManager)
        isinstance(menuBuilder, IMenuBuilder)
        isinstance(artistDetector, IArtistDetector)
        isinstance(artistComparer, IComparer)
        isinstance(output, IOutput)

        self.menuManager = menuManager
        self.menuBuilder = menuBuilder
        self.output = output

        self.artistCompareCommand = TryExceptCommandDecorator(ArtistCompareCommand(artistDetector, artistComparer), output)
        self.detectNewAlbumsCommand = TryExceptCommandDecorator(DetectNewAlbumsCommand(artistDetector, artistComparer), output)

    def start(self):
        emptyCommand = NotImplementedCommand(self.output)

        self.menuBuilder.addMenuItem("ArtistComparer", lambda b: {
                b.addMenuCommand("Compare all", self.artistCompareCommand),
                b.addMenuCommand("Compare only album amount", emptyCommand)
            }).finish()

        self.menuBuilder.addMenuCommand("Detect Spotify Albums", self.detectNewAlbumsCommand)

        self.menuBuilder.addMenuItem("AppData configuration", lambda b: {
                b.addMenuCommand("Reset AppData", emptyCommand),
                b.addMenuItem("Set properties", lambda x: {
                    b.addMenuCommand("Set 'fileBasePath'", emptyCommand),
                    b.addMenuCommand("Set 'baseComparerType'", emptyCommand),
                    b.addMenuCommand("Set 'compareComparerType'", emptyCommand)
                })
            }).finish()

        self.menuManager.show(self.menuBuilder)


class NotImplementedCommand(ICommand):

    def __init__(self, output: IOutput):
        isinstance(output, IOutput)

        self.output = output

    def execute(self):
        self.output.send("Command not implemented!", 0)

    def waitAfterSucceeded(self) -> bool:
        return True
