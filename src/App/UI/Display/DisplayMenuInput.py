from ...CrossCutting.Interaction_Contract.ISelector import ISelector
from ...CrossCutting.Interaction_Contract.IInput import IInput
from ...CrossCutting.Interaction_Contract.InvalidSelectionError import InvalidSelectionError
from ..Menu_Contract.IMenuInput import IMenuInput
from ..Menu_Contract.MenuSelectionError import MenuSelectionError


class DisplayMenuInput(IMenuInput):

    def __init__(
            self,
            selector: ISelector,
            input: IInput):
        isinstance(selector, ISelector)
        isinstance(input, IInput)

        self.selector = selector
        self.input = input

    def getSelection(self) -> int:
        try:
            return self.selector.silentChoose()
        except InvalidSelectionError:
            raise MenuSelectionError("Invalid value selected!")

    def waitForInput(self) -> int:
        return self.input.waitForInput()
