from ...CrossCutting.Interaction_Contract.IOutput import IOutput
from ..Menu_Contract.IMenuOutput import IMenuOutput


class DisplayMenuOutput(IMenuOutput):

    def __init__(
            self,
            output: IOutput):
        isinstance(output, IOutput)

        self.output = output

    def printItem(self, value: str):
        self.output.send(value, 1)

    def printHeader(self, value: str):
        self.output.send(value, 0)

    def print(self, value: str):
        self.output.send(value, 0)

    def newPage(self):
        self.output.clear()
