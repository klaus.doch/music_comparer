from ..Menu_Contract.IMenuBuilder import IMenuBuilder, MenuBuilderError
from ..Menu_Contract.IMenuItem import IMenuItem
from ..Menu_Contract.ICommand import ICommand
from ..Menu_Contract.IMenuManager import IMenuManager
from .MenuHeader import MenuHeader
from .MenuCommand import MenuCommand

from typing import List, Callable, Any


class MenuBuilder(IMenuBuilder):

    def __init__(self):
        self.menuItems: List[IMenuItem] = []
        self.actualMenuItem: IMenuItem = None

    def addMenuItem(self, header: str, itemsCreation: Callable[[IMenuBuilder], Any]) -> IMenuBuilder:
        if not header or not itemsCreation:
            raise ValueError()

        if self.actualMenuItem:
            self.oldMenuItem = self.actualMenuItem.items
        else:
            self.oldMenuItem = self.menuItems

        self.actualMenuItem = MenuHeader(header)
        self.oldMenuItem.append(self.actualMenuItem)

        itemsCreation(self)

        if not self.actualMenuItem.items or len(self.actualMenuItem.items) == 0:
            raise MenuBuilderError("No childs for menu item created")

        return self

    def finish(self):
        self.actualMenuItem = None

    def addMenuCommand(self, header: str, command: ICommand):
        if self.actualMenuItem:
            self.actualMenuItem.items.append(MenuCommand(header, command))
        else:
            self.menuItems.append(MenuCommand(header, command))

    def build(self, manager: IMenuManager) -> IMenuItem:
        self.addMenuCommand("Exit", ExitCommand(manager))

        menu = MenuHeader("Main Menu", self.menuItems)
        self.addBackCommands(menu)
        return menu

    def addBackCommands(self, menu: MenuHeader):
        for item in menu.items:
            if isinstance(item, MenuHeader):

                for subItem in item.items:
                    if isinstance(subItem, MenuHeader):
                        self.addBackCommands(item)

                item.items.append(MenuHeader(menu.header, menu.items))


class ExitCommand(ICommand):

    def __init__(self, menuManager: IMenuManager):
        isinstance(menuManager, IMenuManager)

        self.menuManager = menuManager

    def execute(self):
        self.menuManager.exit()

    def waitAfterSucceeded(self) -> bool:
        return False
