from dataclasses import dataclass

from ..Menu_Contract.IMenuItem import IMenuItem
from ..Menu_Contract.ICommand import ICommand


@dataclass
class MenuCommand(IMenuItem):
    header: str
    command: ICommand
