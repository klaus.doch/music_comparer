from dataclasses import dataclass, field
from typing import List

from ..Menu_Contract.IMenuItem import IMenuItem


@dataclass
class MenuHeader(IMenuItem):
    header: str
    items: List['IMenuItem'] = field(default_factory=list)
