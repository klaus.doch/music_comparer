from ..Menu_Contract.IMenuManager import IMenuManager
from ..Menu_Contract.IMenuBuilder import IMenuBuilder
from ..Menu_Contract.IMenuOutput import IMenuOutput
from ..Menu_Contract.IMenuInput import IMenuInput
from ..Menu_Contract.MenuSelectionError import MenuSelectionError
from ..Menu_Contract.MenuError import MenuError
from .MenuHeader import MenuHeader
from .MenuCommand import MenuCommand


class MenuManager(IMenuManager):

    def __init__(
            self,
            output: IMenuOutput,
            input: IMenuInput):
        isinstance(output, IMenuOutput)
        isinstance(input, IMenuInput)

        self.output = output
        self.input = input
        self.actualMenu = None
        self.exitFlag = False

    def show(self, builder: IMenuBuilder):
        isinstance(builder, IMenuBuilder)

        self.actualMenu = builder.build(self)
        while not self.exitFlag:
            self.draw()
            self.askUser()

    def exit(self):
        self.exitFlag = True

    def draw(self):
        self.output.newPage()

        self.output.printHeader(self.actualMenu.header)

        index = 1
        for item in self.actualMenu.items:
            self.output.printItem(f"{index}. {item.header}")
            index += 1

    def askUser(self):
        try:
            selection = self.input.getSelection()
        except MenuSelectionError as error:
            self.output.newPage()
            self.output.print(str(error.args))
            self.input.waitForInput()
            return

        try:
            selectedItem = self.actualMenu.items[selection]
        except IndexError:
            self.output.newPage()
            self.output.print("Invalid menu selection!")
            self.input.waitForInput()
            return

        self.output.newPage()

        if isinstance(selectedItem, MenuCommand):
            selectedItem.command.execute()

            if selectedItem.command.waitAfterSucceeded():
                self.output.print("Command successfully executed. Press key to continue...")
                self.input.waitForInput()

        elif isinstance(selectedItem, MenuHeader):
            self.actualMenu = selectedItem

        else:
            raise MenuError(f"MenuItem-Type '{type(selectedItem)}' not handled.")
