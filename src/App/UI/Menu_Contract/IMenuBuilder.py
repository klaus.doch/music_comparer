from abc import ABC, abstractmethod
from typing import Callable, Any

from .IMenuItem import IMenuItem
from .ICommand import ICommand


class IMenuBuilder(ABC):

    @abstractmethod
    def addMenuItem(self, header: str, itemsCreation: Callable[['IMenuBuilder'], Any]) -> 'IMenuBuilder':
        """Can raise 'MenuBuilderError'"""
        pass

    @abstractmethod
    def addMenuCommand(self, header: str, command: ICommand) -> 'IMenuBuilder':
        pass

    @abstractmethod
    def finish(self):
        pass

    @abstractmethod
    def build(self) -> IMenuItem:
        pass


class MenuBuilderError(Exception):
    """Raised when the builder crashes"""
    pass
