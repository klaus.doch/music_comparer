from abc import ABC, abstractmethod


class IMenuInput(ABC):

    @abstractmethod
    def getSelection(self) -> int:
        """Can raise MenuSelectionError"""
        pass

    @abstractmethod
    def waitForInput(self) -> int:
        pass
