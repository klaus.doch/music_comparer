from abc import ABC, abstractmethod

from .IMenuBuilder import IMenuBuilder


class IMenuManager(ABC):

    @abstractmethod
    def show(self, builder: IMenuBuilder):
        pass

    @abstractmethod
    def exit(self):
        pass
