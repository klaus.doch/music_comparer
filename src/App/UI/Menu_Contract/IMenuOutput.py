from abc import ABC, abstractmethod


class IMenuOutput(ABC):

    @abstractmethod
    def printItem(self, value: str):
        pass

    @abstractmethod
    def printHeader(self, value: str):
        pass

    @abstractmethod
    def print(self, value: str):
        pass

    @abstractmethod
    def newPage(self):
        pass
