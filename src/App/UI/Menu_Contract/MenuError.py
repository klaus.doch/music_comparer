class MenuError(Exception):
    """Raise when the menu crashes."""
    pass
