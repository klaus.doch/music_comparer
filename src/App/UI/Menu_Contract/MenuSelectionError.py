class MenuSelectionError(Exception):
    """Raise when selection of menu item failed."""
    pass
