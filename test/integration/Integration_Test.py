import unittest
import responses
import json

from src.App.App import App


class BasicTest(unittest.TestCase):

    def setUp(self):
        setUpResponses("basic")
        self.app = App("test/integration/fixture/basic/appData.json")

    @responses.activate
    def test_compare_artistsEqual_noResult(self):
        self.app.display.artistCompareCommand.execute()
        results = self.app.display.artistCompareCommand.command.lastResults
        self.assertEqual(len(results), 1)
        self.assertEqual(len(results[0].items), 0)


class ComplexTest(unittest.TestCase):

    def setUp(self):
        setUpResponses("complex")
        self.app = App("test/integration/fixture/complex/appData.json")

    @responses.activate
    def test_compare_artistsNotEqual_result(self):
        self.app.display.artistCompareCommand.execute()
        results = self.app.display.artistCompareCommand.command.lastResults
        self.assertEqual(len(results), 1)
        self.assertEqual(len(results[0].items), 3)


def setUpResponses(type: str):
    # Fake Spotify-Responses
    with open('./test/integration/fixture/token.json') as f:
        token = json.load(f)
    responses.add(
        responses.Response(
            method='POST',
            json=token,
            url='https://accounts.spotify.com/api/token',
        )
    )
    with open('./test/integration/fixture/search.json') as f:
        search = json.load(f)
    responses.add(
        responses.Response(
            method='GET',
            json=search,
            url='https://api.spotify.com/v1/search',
        )
    )
    with open('./test/integration/fixture/artist.json') as f:
        artist = json.load(f)
    responses.add(
        responses.Response(
            method='GET',
            json=artist,
            url='https://api.spotify.com/v1/artists/123/albums',
        )
    )
    with open(f'./test/integration/fixture/{type}/album1.json') as f:
        album = json.load(f)
    responses.add(
        responses.Response(
            method='GET',
            json=album,
            url='https://api.spotify.com/v1/albums/1234/tracks/',
        )
    )
    with open(f'./test/integration/fixture/{type}/album2.json') as f:
        album = json.load(f)
    responses.add(
        responses.Response(
            method='GET',
            json=album,
            url='https://api.spotify.com/v1/albums/1235/tracks/',
        )
    )


if __name__ == '__main__':
    unittest.main()
