import unittest
from typing import List

from src.App.Business.ArtistComparer.AlbumComparer.AlbumComparer import AlbumComparer
from src.App.Business.ArtistComparer_Contract.DataClasses.Album import Album
from src.App.Business.ArtistComparer_Contract.DataClasses.Track import Track


class AlbumComparerTest(unittest.TestCase):

    # isTrackAmountEqual
    def test_isTrackAmountEqual_emptyArguments_assertionError(self):
        sut = self.createSut()

        with self.assertRaises(AssertionError):
            sut.isTrackAmountEqual(None, None)

    def test_isTrackAmountEqual_differentAlbumName_ValueError(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.name = "OtherAlbum"

        with self.assertRaises(ValueError):
            sut.isTrackAmountEqual(base, compare)

    def test_isTrackAmountEqual_sameLength_true(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()

        result = sut.isTrackAmountEqual(base, compare)

        self.assertTrue(result)

    def test_isTrackAmountEqual_oneMoreTrackInCompare_false(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        base.tracks.pop(0)

        result = sut.isTrackAmountEqual(base, compare)

        self.assertFalse(result)

    def test_isTrackAmountEqual_twoMoreTracksInCompare_false(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        base.tracks.pop(0)
        base.tracks.pop(0)

        result = sut.isTrackAmountEqual(base, compare)

        self.assertFalse(result)

    def test_isTrackAmountEqual_oneMoreTrackInBase_false(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks.pop(0)

        result = sut.isTrackAmountEqual(base, compare)

        self.assertFalse(result)

    def test_isTrackAmountEqual_twoMoreTracksInBase_false(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks.pop(0)
        compare.tracks.pop(0)

        result = sut.isTrackAmountEqual(base, compare)

        self.assertFalse(result)

    # getDifferentTrackNames
    def test_getDifferentTrackNames_emptyArguments_assertionError(self):
        sut = self.createSut()

        with self.assertRaises(AssertionError):
            sut.getDifferentTrackNames(None, None)

    def test_getDifferentTrackNames_differentAlbumName_ValueError(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.name = "OtherAlbum"

        with self.assertRaises(ValueError):
            sut.getDifferentTrackNames(base, compare)

    def test_getDifferentTrackNames_sameAlbum_noResult(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()

        results = sut.getDifferentTrackNames(base, compare)

        self.assertEqual(len(results), 0)

    def test_getDifferentTrackNames_oneDifferentTrackName_result(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks[1] = Track("TestTrack", 20)

        results = sut.getDifferentTrackNames(base, compare)

        self.assertEqual(len(results), 1)

    def test_getDifferentTrackNames_twoDifferentTrackNames_result(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks[0] = Track("TestTrack", 20)
        compare.tracks[1] = Track("TestTrack2", 20)

        results = sut.getDifferentTrackNames(base, compare)

        self.assertEqual(len(results), 2)

    # getDifferentTrackLengths
    def test_getDifferentTrackLengths_emptyArguments_valueError(self):
        sut = self.createSut()

        with self.assertRaises(AssertionError):
            sut.getDifferentTrackLengths(None, None)

    def test_getDifferentTrackLengths_differentAlbumName_ValueError(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.name = "OtherAlbum"

        with self.assertRaises(ValueError):
            sut.getDifferentTrackLengths(base, compare)

    def test_getDifferentTrackLengths_validArguments_noResult(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()

        results = sut.getDifferentTrackLengths(base, compare)

        self.assertEqual(len(results), 0)

    def test_getDifferentTrackLengths_trackLengthIsDifferent_result(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks[0].duration = 18000

        results = sut.getDifferentTrackLengths(base, compare)

        self.assertEqual(len(results), 1)

    def test_getDifferentTrackLengths_twoTrackLengthsAreDifferent_result(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks[0].duration = 18000
        base.tracks[1].duration = 22000

        results = sut.getDifferentTrackLengths(base, compare)

        self.assertEqual(len(results), 2)

    def test_getDifferentTrackLengths_trackLengthIsDifferentButInTolerance_noResult(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks[0].duration = 19500

        results = sut.getDifferentTrackLengths(base, compare)

        self.assertEqual(len(results), 0)

    def test_getDifferentTrackLengths_compareTrackNotFound_noResult(self):
        sut = self.createSut()
        base = self.getDefaultAlbum()
        compare = self.getDefaultAlbum()
        compare.tracks.pop(0)

        results = sut.getDifferentTrackLengths(base, compare)

        self.assertEqual(len(results), 0)

    # fixture
    def createSut(self) -> AlbumComparer:
        return AlbumComparer()

    def getDefaultAlbum(self) -> Album:
        album = Album("TestAlbum")
        album.tracks = self.getDefaultTrackList()
        return album

    def getDefaultTrackList(self) -> List[Track]:
        return [Track("Track1", 20000), Track("Track2", 20000), Track("Track3", 20000)]


if __name__ == '__main__':
    unittest.main()
