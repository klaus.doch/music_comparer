import unittest
from unittest.mock import MagicMock

from src.App.Business.ArtistComparer.ArtistComparer import ArtistComparer
from src.App.Business.ArtistComparer.ArtistLoader.IArtistLoaderFactory import IArtistLoaderFactory
from src.App.Business.ArtistComparer.ArtistLoader.IArtistLoader import IArtistLoader
from src.App.Business.ArtistComparer.AlbumComparer.IAlbumComparer import IAlbumComparer
from src.App.Business.ArtistComparer_Contract.DataClasses.Artist import Artist
from src.App.Business.ArtistComparer_Contract.DataClasses.Album import Album


class ArtistComparerTest(unittest.TestCase):

    def test_constructor_emptyArguments_assertionError(self):
        with self.assertRaises(AssertionError):
            ArtistComparer(None, None)

    def test_compare_emptyArguments_valueError(self):
        sut = self.createSut()
        with self.assertRaises(ValueError):
            sut.compareAll(None)

    def test_compare_compareAlbumNotFound_result(self):
        loaderFactory = self.getDefaultLoaderFactory()
        loader = MagicMock(spec=IArtistLoader)
        artist = self.getDefaultArtist()
        artist.albums.pop(0)
        loader.load.return_value = artist
        loaderFactory.getCompareLoader.return_value = loader
        sut = self.createSut(loaderFactory=loaderFactory)

        result = sut.compareAll("TestArtist")

        self.assertEqual(len(result.items), 1)

    def test_compare_ambigiousCompareAlbum_ValueError(self):
        loaderFactory = self.getDefaultLoaderFactory()
        loader = MagicMock(spec=IArtistLoader)
        artist = self.getDefaultArtist()
        artist.albums.append(Album("Album1"))
        loader.load.return_value = artist
        loaderFactory.getCompareLoader.return_value = loader
        sut = self.createSut(loaderFactory=loaderFactory)

        with self.assertRaises(ValueError):
            sut.compareAll("TestArtist")

    def test_compare_albumsEqual_noResult(self):
        sut = self.createSut()

        result = sut.compareAll("TestArtist")

        self.assertCountEqual(result.items, [])

    def test_compare_trackAmountIsNotEqual_result(self):
        albumComparer = self.getDefaultAlbumComparer()
        albumComparer.isTrackAmountEqual.return_value = False
        sut = self.createSut(albumComparer=albumComparer)

        result = sut.compareAll("TestArtist")

        self.assertEqual(len(result.items), 2)

    def test_compare_trackNamesAreNotEqual_result(self):
        albumComparer = self.getDefaultAlbumComparer()
        albumComparer.getDifferentTrackNames.return_value = ["Name falsch1", "Name falsch2"]
        sut = self.createSut(albumComparer=albumComparer)

        result = sut.compareAll("TestArtist")

        self.assertEqual(len(result.items), 4)

    def test_compare_trackLengthsAreNotEqual_result(self):
        albumComparer = self.getDefaultAlbumComparer()
        albumComparer.getDifferentTrackLengths.return_value = ["Länge falsch1", "Länge falsch2"]
        sut = self.createSut(albumComparer=albumComparer)

        result = sut.compareAll("TestArtist")

        self.assertEqual(len(result.items), 4)

    # fixture
    def createSut(
            self,
            loaderFactory=None,
            albumComparer=None) -> ArtistComparer:

        if loaderFactory is None:
            loaderFactory = self.getDefaultLoaderFactory()

        if albumComparer is None:
            albumComparer = self.getDefaultAlbumComparer()

        return ArtistComparer(
            loaderFactory,
            albumComparer)

    def getDefaultAlbumComparer(self) -> IAlbumComparer:
        albumComparer = MagicMock(spec=IAlbumComparer)
        albumComparer.isTrackAmountEqual.return_value = True
        albumComparer.getDifferentTrackNames.return_value = []
        albumComparer.getDifferentTrackLengths.return_value = []
        return albumComparer

    def getDefaultLoaderFactory(self) -> IArtistLoaderFactory:
        loaderFactory = MagicMock(spec=IArtistLoaderFactory)
        loader = MagicMock(spec=IArtistLoader)
        loader.load.return_value = self.getDefaultArtist()

        loaderFactory.getBaseLoader.return_value = loader
        loaderFactory.getCompareLoader.return_value = loader

        return loaderFactory

    def getDefaultArtist(self) -> Artist:
        artist = Artist("TestArtist")
        albums = [Album("Album1"), Album("Album2")]
        artist.albums = albums
        return artist


if __name__ == '__main__':
    unittest.main()
