import unittest
from unittest.mock import MagicMock

from src.App.Business.ArtistComparer.ArtistLoader.FileArtistLoader import FileArtistLoader
from src.App.CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from src.App.DataAccess.FileAccess_Contracts.IFileAccess import IFileAccess
from src.App.DataAccess.Audio_Contracts.IAudio import IAudio


class FileArtistLoaderTest(unittest.TestCase):

    def test_constructor_emptyArguments_assertionError(self):
        with self.assertRaises(AssertionError):
            FileArtistLoader(None, None, None)

    def test_load_emptyArguments_valueError(self):
        sut = self.createSut()
        with self.assertRaises(ValueError):
            sut.load(None)

    def test_load_validArguments_dataContainerReturned(self):
        sut = self.createSut()

        retVal = sut.load("artist")

        self.assertEqual(len(retVal.albums), 2)
        self.assertEqual(len(retVal.albums[0].tracks), 2)

    def test_load_validArguments_trackNamesCutted(self):
        sut = self.createSut()

        retVal = sut.load("artist")

        self.assertEqual(retVal.albums[0].tracks[0].name, "KEIN BOCK")

    # fixture
    def createSut(
            self,
            adr=None,
            fileAccess=None,
            audio=None) -> FileArtistLoader:

        if adr is None:
            adr = MagicMock(spec=IAppDataResolver)

        if fileAccess is None:
            fileAccess = self.getDefaultFileAccess()

        if audio is None:
            audio = MagicMock(spec=IAudio)

        return FileArtistLoader(adr, fileAccess, audio)

    def getDefaultFileAccess(self) -> IFileAccess:
        fileAccess = MagicMock(spec=IFileAccess)

        fileAccess.getDirectories.return_value = ["KEIN BOCK", "No face no name"]
        fileAccess.getFilesByPattern.return_value = ["01 KEIN BOCK.m4a", "02 bla.m4a"]

        return fileAccess


if __name__ == '__main__':
    unittest.main()
