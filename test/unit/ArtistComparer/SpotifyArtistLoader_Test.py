import unittest
from unittest.mock import MagicMock

from src.App.Business.ArtistComparer.ArtistLoader.SpotifyArtistLoader import SpotifyArtistLoader
from src.App.Business.ArtistComparer_Contract.Exceptions.ArtistNotFoundError import ArtistNotFoundError
from src.App.CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from src.App.CrossCutting.Interaction_Contract.ISelector import ISelector
from src.App.DataAccess.Spotify_Contracts.ISpotify import ISpotify
from src.App.DataAccess.Spotify_Contracts.DataClasses import SearchResult, Album, Track


class SpotifyArtistLoaderTest(unittest.TestCase):

    def test_constructor_emptyArguments_assertionError(self):
        with self.assertRaises(AssertionError):
            SpotifyArtistLoader(None, None, None)

    def test_load_emptyArguments_valueError(self):
        sut = self.createSut()
        with self.assertRaises(ValueError):
            sut.load(None)

    def test_load_validArguments_dataContainerReturned(self):
        sut = self.createSut()
        albums = ["TestAlbum", "TestAlbum2"]
        tracks = ["TestTrack", "TestTrack2"]

        result = sut.load("artist")

        resultAlbumNames = list(map(lambda x: x.name, result.albums))
        resultTrackNames = list(map(lambda x: x.name, result.albums[0].tracks))
        self.assertCountEqual(resultAlbumNames, albums)
        self.assertListEqual(resultAlbumNames, albums)
        self.assertCountEqual(resultTrackNames, tracks)
        self.assertListEqual(resultTrackNames, tracks)

    def test_load_ambigiousArtist_SecondSelected(self):
        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = [
            SearchResult("1234", "TestArtist"),
            SearchResult("1235", "TestArtist2")]
        spotifyMock.getAlbums.return_value = [
            Album("1235", "TestAlbum"),
            Album("1236", "TestAlbum2")]
        spotifyMock.getTracks.side_effect = [
            [Track("1237", "TestTrack", 20), Track("1238", "TestTrack2", 20)],
            [Track("1239", "TestTrack3", 20), Track("1240", "TestTrack4", 20)]]
        sut = self.createSut(spotify=spotifyMock)

        result = sut.load("artist")

        spotifyMock.getAlbums.assert_called_once_with("1235")
        self.assertEqual(len(result.albums), 2)

    def test_load_noArtistFound_ArtistNotFoundErrorRaised(self):
        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = {}
        sut = self.createSut(spotify=spotifyMock)

        with self.assertRaises(ArtistNotFoundError):
            sut.load("artist")

    def test_load_artistContainsDuplicate_ReturnOnlyFullAlbum(self):
        def getAlbums(value: int):
            if value == "1235":
                return [
                    Track("1237", "TestTrack", 20),
                    Track("1238", "TestTrack2", 20),
                    Track("1239", "TestTrack3", 20)]
            elif value == "1236":
                return [Track("1238", "TestTrack2", 20)]

        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = [SearchResult("1234", "TestArtist")]
        spotifyMock.getAlbums.return_value = [
            Album("1235", "TestAlbum"),
            Album("1236", "TestSingle")]
        spotifyMock.getTracks.side_effect = getAlbums
        sut = self.createSut(spotify=spotifyMock)

        result = sut.load("artist")

        resultTrackNames = list(map(lambda x: x.name, result.albums[0].tracks))
        self.assertEqual(len(result.albums), 1)
        self.assertListEqual(resultTrackNames, ["TestTrack", "TestTrack2", "TestTrack3"])

    def test_load_artistContainsDuplicates_ReturnOnlyFullAlbums(self):
        def getAlbums(value: int):
            if value == "1135":
                return [
                    Track("1237", "TestTrack", 20),
                    Track("1238", "TestTrack2", 20),
                    Track("1239", "TestTrack3", 20)]
            elif value == "1136":
                return [Track("1238", "TestTrack2", 20)]
            elif value == "1137":
                return [
                    Track("1239", "TestTrack5", 20),
                    Track("1241", "TestTrack4", 20)]
            elif value == "1138":
                return [
                    Track("1240", "TestTrack5", 20),
                    Track("1241", "TestTrack4", 20),
                    Track("1242", "TestTrack6", 20)]

        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = [SearchResult("1234", "TestArtist")]
        spotifyMock.getAlbums.return_value = [
            Album("1135", "TestAlbum"),
            Album("1136", "TestSingle"),
            Album("1137", "TestSingle2"),
            Album("1138", "TestAlbum2")]
        spotifyMock.getTracks.side_effect = getAlbums
        sut = self.createSut(spotify=spotifyMock)

        result = sut.load("artist")

        resultTrackNamesZero = list(map(lambda x: x.name, result.albums[0].tracks))
        resultTrackNamesOne = list(map(lambda x: x.name, result.albums[1].tracks))
        self.assertEqual(len(result.albums), 2)
        self.assertListEqual(resultTrackNamesZero, ["TestTrack", "TestTrack2", "TestTrack3"])
        self.assertListEqual(resultTrackNamesOne, ["TestTrack5", "TestTrack4", "TestTrack6"])

    def test_load_albumContainsDuplicatesWithDifferentDuration_ReturnBoth(self):
        def getAlbums(value: int):
            if value == "1235":
                return [
                    Track("1237", "TestTrack", 20000),
                    Track("1238", "TestTrack2", 20000),
                    Track("1239", "TestTrack3", 20000)]
            elif value == "1236":
                return [Track("1238", "TestTrack2", 10000)]

        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = [SearchResult("1234", "TestArtist")]
        spotifyMock.getAlbums.return_value = [
            Album("1235", "TestAlbum"),
            Album("1236", "TestSingle")]
        spotifyMock.getTracks.side_effect = getAlbums
        sut = self.createSut(spotify=spotifyMock)

        result = sut.load("artist")

        self.assertEqual(len(result.albums), 2)

    def test_load_albumContainsDuplicatesWithLowDifferentDuration_ReturnFullAlbum(self):
        def getAlbums(value: int):
            if value == "1235":
                return [
                    Track("1237", "TestTrack", 20000),
                    Track("1238", "TestTrack2", 20000),
                    Track("1239", "TestTrack3", 20000)]
            elif value == "1236":
                return [Track("1238", "TestTrack2", 16000)]

        spotifyMock = MagicMock(spec=ISpotify)
        spotifyMock.searchArtist.return_value = [SearchResult("1234", "TestArtist")]
        spotifyMock.getAlbums.return_value = [
            Album("1235", "TestAlbum"),
            Album("1236", "TestSingle")]
        spotifyMock.getTracks.side_effect = getAlbums
        sut = self.createSut(spotify=spotifyMock)

        result = sut.load("artist")

        resultTrackNames = list(map(lambda x: x.name, result.albums[0].tracks))
        self.assertEqual(len(result.albums), 1)
        self.assertListEqual(resultTrackNames, ["TestTrack", "TestTrack2", "TestTrack3"])

    # fixture
    def createSut(
            self,
            adr=None,
            spotify=None,
            selector=None) -> SpotifyArtistLoader:

        if adr is None:
            adr = MagicMock(spec=IAppDataResolver)

        if spotify is None:
            spotify = self.getDefaultSpotify()

        if selector is None:
            selector = self.getDefaultSelector()

        return SpotifyArtistLoader(adr, spotify, selector)

    def getDefaultSpotify(self) -> ISpotify:
        spotify = MagicMock(spec=ISpotify)

        spotify.searchArtist.return_value = [SearchResult("1234", "TestArtist")]
        spotify.getAlbums.return_value = [Album("1235", "TestAlbum"), Album("1236", "TestAlbum2")]
        spotify.getTracks.side_effect = [
            [Track("1237", "TestTrack", 20), Track("1238", "TestTrack2", 20)],
            [Track("1239", "TestTrack3", 20), Track("1240", "TestTrack4", 20)]]

        return spotify

    def getDefaultSelector(self) -> ISelector:
        selector = MagicMock(spec=ISelector)

        selector.choose.return_value = 1

        return selector


if __name__ == '__main__':
    unittest.main()
