import unittest
from unittest.mock import MagicMock

from src.App.Business.ArtistDetector.FileArtistDetector import FileArtistDetector
from src.App.CrossCutting.AppData_Contracts.IAppDataResolver import IAppDataResolver
from src.App.DataAccess.FileAccess_Contracts.IFileAccess import IFileAccess


class FileArtistDetectorTest(unittest.TestCase):

    def test_constructor_emptyArguments_assertionError(self):
        with self.assertRaises(AssertionError):
            FileArtistDetector(None, None)

    def test_getArtists_validArguments_artistsReturned(self):
        sut = self.createSut()
        artists = ["LUVRE47", "PTK"]

        result = sut.getArtists()

        self.assertCountEqual(result, artists)
        self.assertListEqual(result, artists)

    def createSut(
            self,
            adr=None,
            fileAccess=None) -> FileArtistDetector:

        if adr is None:
            adr = MagicMock(spec=IAppDataResolver)

        if fileAccess is None:
            fileAccess = self.getDefaultFileAccess()

        return FileArtistDetector(adr, fileAccess)

    def getDefaultFileAccess(self) -> IFileAccess:
        fileAccess = MagicMock(spec=IFileAccess)

        fileAccess.getDirectories.return_value = ["LUVRE47", "PTK"]

        return fileAccess


if __name__ == '__main__':
    unittest.main()
