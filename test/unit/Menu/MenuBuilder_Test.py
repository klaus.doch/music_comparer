import unittest
from unittest.mock import MagicMock

from src.App.UI.Menu.MenuBuilder import MenuBuilder
from src.App.UI.Menu_Contract.ICommand import ICommand
from src.App.UI.Menu_Contract.IMenuBuilder import MenuBuilderError


class MenuBuilderTest(unittest.TestCase):

    def test_addMenuItem_emptyArguments_valueError(self):
        sut = self.createSut()
        with self.assertRaises(ValueError):
            sut.addMenuItem(None, None).finish()

    def test_addMenuItem_menuItemWithOneChild_correctMenu(self):
        sut = self.createSut()

        sut.addMenuItem("TestItem", lambda b: b.addMenuCommand("header", MagicMock(spec=ICommand))).finish()

        self.assertEqual(len(sut.menuItems), 1)
        self.assertEqual(len(sut.menuItems[0].items), 1)

    def test_addMenuItem_menuItemNoChildAdded_MenuBuilderError(self):
        sut = self.createSut()

        with self.assertRaises(MenuBuilderError):
            sut.addMenuItem("TestItemX", lambda do: do == "nothing").finish()

    def test_addMenuItem_menuItemWithTwoChilds_correctMenu(self):
        sut = self.createSut()

        sut.addMenuItem("TestItem", lambda b: {
                b.addMenuCommand("header", MagicMock(spec=ICommand)),
                b.addMenuCommand("header2", MagicMock(spec=ICommand))
            }).finish()

        self.assertEqual(len(sut.menuItems), 1)
        self.assertEqual(len(sut.menuItems[0].items), 2)

    def test_addMenuItem_menuItemWithOneChildAndTwoSubChils_correctMenu(self):
        sut = self.createSut()

        sut.addMenuItem("TestItem", lambda b: {
                b.addMenuItem("header", lambda b: {
                    b.addMenuCommand("header1", MagicMock(spec=ICommand)),
                    b.addMenuCommand("header2", MagicMock(spec=ICommand))
                }),
            }).finish()

        self.assertEqual(len(sut.menuItems), 1)
        self.assertEqual(len(sut.menuItems[0].items), 1)
        self.assertEqual(len(sut.menuItems[0].items[0].items), 2)

    def test_addMenuItem_twoMenuItemsWithOneChild_correctMenu(self):
        sut = self.createSut()

        sut.addMenuItem("TestItem1", lambda b: {
                b.addMenuCommand("header", MagicMock(spec=ICommand))
            }).finish()
        sut.addMenuItem("TestItem2", lambda b: {
                b.addMenuCommand("header", MagicMock(spec=ICommand))
            })

        self.assertEqual(len(sut.menuItems), 2)
        self.assertEqual(len(sut.menuItems[0].items), 1)
        self.assertEqual(len(sut.menuItems[1].items), 1)

    # fixture
    def createSut(self) -> MenuBuilder:
        return MenuBuilder()


if __name__ == '__main__':
    unittest.main()
